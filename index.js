#! /usr/bin/env node

const { spawn } = require('child_process');
const {promisify} = require("util");
const path =  require('path');
const pkg =  require("./templates/pkg");
const app =  require("./templates/App");
const cwd = process.cwd();
const fs =  require('fs');

const name = process.argv[2];


function isValidPackageName(projectName) {
  return /^(?:@[a-z0-9-*~][a-z0-9-*._~]*\/)?[a-z0-9-~][a-z0-9-._~]*$/.test(
    projectName
  );
}


if (!isValidPackageName(name)) {
  console.log(`
  Invalid directory name.
  Usage: create-vue-node name-of-project  
`);

  return false;
}

const repoURL = 'https://gitlab.com/b.sunday/vue-nodejs-starter.git';
const root = path.join(cwd, name);

runCommand('git', ['clone', repoURL, name])
  .then(() => {
    fs.writeFileSync(
      path.resolve(root, "package.json"),
      JSON.stringify(pkg(name), null, 2)
    );
    return runCommand('rm', ['-rf', `${name}/.git`]);
  }).then(() => {
    
    console.log('Installing dependencies backend...');

    return runCommand('yarn', ['install'], {
      cwd: process.cwd() + '/' + name  
    })
  }).then(()=>{
    console.log('Installing dependencies frontend...');
     fs.writeFileSync(
       path.resolve(`${root}/frontend/src`, "App.vue"),
       app(name)
     );
    return runCommand('yarn', ['install'], {
      cwd: process.cwd() + '/' + name  + '/' + 'frontend'
    })
  })
  // .then(()=>{
  //   console.log('Almost done...');
      

  //   return runCommand("yarn", ["install"], {
  //     cwd: process.cwd() + "/" + name + "/" + "frontend" +"/src",
  //   });
  // })
  .then(() => {
    console.log('Done! 🏁');
    console.log('');
    console.log('To get started:');
    console.log('cd', name);
    console.log('make dev');
  });

function runCommand(command, args, options = undefined) {
  const spawned = spawn(command, args, options);

  return new Promise((resolve) => {
    spawned.stdout.on('data', (data) => {
      console.log(data.toString());
    });
    
    spawned.stderr.on('data', (data) => {
      console.error(data.toString());
    });
    
    spawned.on('close', () => {
      resolve();
    });
  });
}
