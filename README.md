# create-vue-node

A CLI to automatically clone the [VUE NODE STARTER](https://gitlab.com/b.sunday/vue-nodejs-starter.git).

## Installation

Install the CLI globally OR use npx:

```sh
npm install -g create-vue-node
```

## Usage

```sh
# with global install
create-vue-node name-of-app
# with npx
npx create-vue-node name-of-app
```

This will create a directory with the given name, clone the [VUE NODE STARTER](https://gitlab.com/b.sunday/vue-nodejs-starter.git) repo into it, and install dependencies.


## Inspired

```by
  CJ R. <cj@null.computer> (https://w3cj.now.sh)
```