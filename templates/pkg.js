module.exports = (name) => {
  return {
      name: `${name}`,
      version: "0.0.0",
      private: true,
      scripts: {
        start: "node ./bin/www",
        server: "nodemon ./bin/www",
        client: "cd frontend && yarn run serve",
        clientinstall: "yarn start --prefix frontend",
        dev: 'concurrently "yarn run server" "yarn run client"',
      },
      dependencies: {
        axios: "^0.26.1",
        bull: "^4.7.0",
        "cookie-parser": "~1.4.4",
        debug: "~2.6.9",
        dotenv: "^16.0.0",
        express: "~4.16.1",
        "http-errors": "~1.6.3",
        jade: "~1.11.0",
        morgan: "~1.9.1",
        pg: "^8.7.3",
        "pg-hstore": "^2.3.4",
        sequelize: "^6.18.0",
      },
      devDependencies: {
        concurrently: "^7.0.0",
        nodemon: "^2.0.15",
      },
      imports: {
        "#server/*": "./server/*.js",
        "#api/*": "./server/api/*.js",
        "#controllers/*": "./server/api/controllers/*.js",
        "#routes/*": "./server/api/routes/*.js",
        "#services/*": "./server/services/*.js",
        "#utils/*": "./server/utils/*.js",
        "#jobs/*": "./server/jobs/*.js",
        "#db/*": "./server/db/*.js",
        "#models/*": "./server/db/models/*.js",
      }
    }
};
